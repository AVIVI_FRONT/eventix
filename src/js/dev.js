
function initMainSlider() {
    $('.main__slider').slick({
        fade: true,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false
    })
}

function openMobmenu() {
    $('.header__mob-menu').on('click', function () {
        $('.header__nav-wrap').toggleClass('opened');
        $('.header__mob-menu').toggleClass('opened');
    })
}

function openTabs() {
    $('.js-tab').not('.active').click(function(){
        var index = $(this).index();
        var content = $('.js-content').eq(index);
        $(this).addClass('active').siblings().removeClass('active');
        $('.js-content').css('display', 'none').eq(index).css('display', 'flex');
    });

    $('.js-tab:first').addClass('active');
    $('.js-content:first').css('display', 'flex');
}

function mobFooterTabs() {
    $('.footer__list-item.has-child').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('show');
        $(this).siblings('.footer__list-item').toggleClass('show');
    })
}

function filterRange() {
    if ($('*').is('.catalog__filter-range-line')) {
        $(".catalog__filter-range-line").slider({
            min: 3000,
            max: 9000,
            values: [4000, 7000],
            range: true,
            animate: "fast",
            slide : function(event, ui) {
                $(".catalog__filter-range-left").val(ui.values[ 0 ]);
                $(".catalog__filter-range-right").val(ui.values[ 1 ]);
            }
        });
        $(".catalog__filter-range-left").val($(".catalog__filter-range-line").slider("values", 0));
        $(".catalog__filter-range-right").val($(".catalog__filter-range-line").slider("values", 1));
        $(document).focusout(function() {
            var input_left = $(".catalog__filter-range-left").val().replace(/[^0-9]/g, ''),
                opt_left = $(".catalog__filter-range-line").slider("option", "min"),
                where_right = $(".catalog__filter-range-line").slider("values", 1),
                input_right = $(".catalog__filter-range-right").val().replace(/[^0-9]/g, ''),
                opt_right = $(".catalog__filter-range-line").slider("option", "max"),
                where_left = $(".catalog__filter-range-line").slider("values", 0);
            if (input_left > where_right) {
                input_left = where_right;
            }
            if (input_left < opt_left) {
                input_left = opt_left;
            }
            if (input_left == "") {
                input_left = 0;
            }
            if (input_right < where_left) {
                input_right = where_left;
            }
            if (input_right > opt_right) {
                input_right = opt_right;
            }
            if (input_right == "") {
                input_right = 0;
            }
            $(".catalog__filter-range-left").val(input_left);
            $(".catalog__filter-range-right").val(input_right);
            $(".catalog__filter-range-line").slider( "values", [ input_left, input_right ] );
        });
    }
}

function initCardSlider() {
    $('.card__slider').slick({
        arrows: false,
        fade: true,
        asNavFor: '.card__slider-small'

    });
    $('.card__slider-small').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.card__slider',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    infinite: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
}

function openCardHall() {
    $('.halls__item').on('click', function () {
        $(this).parents('.halls__wrap').addClass('opened');
        $(this).parents('.halls__wrap').next('.halls__collapse-btn').addClass('opened');
        $(this).parents('.halls__collapse').addClass('opened');
        $(this).parents('.halls__wrap').find('.halls__slider').not('.slick-initialized').slick({
            slidesToShow: 6,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        infinite: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    });
    $('.halls__collapse-btn').on('click', function () {
        $(this).siblings('.halls__wrap').toggleClass('opened');
        $(this).toggleClass('opened');
        $(this).parents('.halls__collapse').toggleClass('opened');
        $(this).siblings('.halls__wrap').find('.halls__slider').not('.slick-initialized').slick({
            slidesToShow: 6,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        infinite: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });

    });
    $('.halls__more').on('click', function () {
        $('.halls__wrap').addClass('opened');
        $('.halls__collapse-btn').addClass('opened');
        $('.halls__collapse').addClass('opened');
        $('.halls__slider').not('.slick-initialized').slick({
            slidesToShow: 6,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        infinite: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    })
}

function resultTabs() {
    $('.result__tabs').on('click', '.result__tab:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.result__content').find('.result__box').removeClass('active').eq($(this).index()).addClass('active');
    });
}

function resultCollpse() {
    $('.js-collapse-btn').on('click', function () {
        $(this).closest('.result__hotel').toggleClass('opened');
    });
}

function resultShowMore () {
    $('.result__halls-more').on('click', function () {
        $(this).siblings('.result__halls').toggleClass('opened').find('.result__hall-detail').removeClass('opened');
    });
}

function resultShowHall() {
    $('.result__hall-title').on('click', function () {
        $(this).parents('.result__hall-detail').toggleClass('opened');
        $(this).parents('.result__halls').addClass('opened');
    });
    $('.result__hall-more').on('click', function () {
        $(this).parents('.result__hall-detail').toggleClass('opened');
        $(this).parents('.result__halls').addClass('opened');
    });
}
function popupMap() {
    $('.card__on-map').on('click', function () {
        // $.fancybox.open(('<div id="popup-map"></div>'), {
        //     toolbar: true
        // });
        initPopupMap();
    })
}

function resultTabSliderReInit() {
    $('.result__tab-list').on('click', function () {
       setTimeout(function () {
           $('.card__slider-small').slick('unslick');
           $('.card__slider-small').slick({
               slidesToShow: 4,
               slidesToScroll: 1,
               asNavFor: '.card__slider',
               responsive: [
                   {
                       breakpoint: 991,
                       settings: {
                           slidesToShow: 3,
                           infinite: true
                       }
                   },
                   {
                       breakpoint: 600,
                       settings: {
                           slidesToShow: 2
                       }
                   },
                   {
                       breakpoint: 480,
                       settings: {
                           slidesToShow: 1
                       }
                   }
               ]
           });
       },10);
    });
}

function openResultFilter() {
    $('.result__mob-filter').on('click', function () {
        $('.catalog__aside').toggleClass('opened');
        if ($('.catalog__aside').hasClass('opened')) {
            $('.overlay').fadeIn();
        } else {
            $('.overlay').fadeOut();
        }

    });
    $('.filter-btn').on('click', function () {
        $('.catalog__aside').toggleClass('opened');
        if ($('.catalog__aside').hasClass('opened')) {
            $('.overlay').fadeIn();
        } else {
            $('.overlay').fadeOut();
        }

    });
    $('.aside__close').on('click', function () {
        $('.catalog__aside').removeClass('opened');
        $('.overlay').fadeOut();
    });
    $('.overlay').on('click', function () {
        $('.catalog__aside').removeClass('opened');
        $('.overlay').fadeOut();
    })
}

function corporateSliderInit() {
    $('.corporate__tabs').slick({
        slidesToShow: 4,
        infinite: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3,
                    arrows: false,
                    centerMode:true,
                    variableWidth: true
                }
            }
        ]
    });
    $('.corporate__slider .slick-prev').on('click', function () {
        $('.corporate__tabs').slick('slickPrev');
        $('.slick-current').click();
    });
    $('.corporate__slider .slick-next').on('click', function () {
        $('.corporate__tabs').slick('slickNext');
        $('.slick-current').click();
    });

    $('.corporate__content.active .corporate__gallery').slick({
        mobileFirst: true,
        slidesToShow: 1,
        infinite: true,
        arrows: true,
        responsive: [
            {
                breakpoint: 769,
                settings: "unslick"
            }
        ]
    })
}

function corporateTabs() {
    $('[data-tab]').on('click', function () {
        $(this).addClass('active').siblings('[data-tab]').removeClass('active');
        $('[data-tab=' + $(this).data('tab') + ']').addClass('active');
        $(this).parents('.corporate__slider').siblings('[data-tab-content=' + $(this).data('tab') + ']').addClass('active').siblings('[data-tab-content]').removeClass('active');
        $(this).parents('.corporate__slider').siblings('[data-tab-content=' + $(this).data('tab') + ']').find('.corporate__gallery').slick({
            mobileFirst: true,
            slidesToShow: 1,
            infinite: true,
            arrows: true,
            responsive: [
                {
                    breakpoint: 769,
                    settings: "unslick"
                }
            ]
        })
    })
}

function eventSliderInit() {
    $('.event__slider').slick();
}

function boxTabsChanger() {
    $(document).on('click', '.js-box-tab', function () {
        var tab = $(this);
        var index = tab.index();
        if (tab.hasClass('active')) {
            return;
        } else {
            $('.js-box-tab').removeClass('active');
            tab.addClass('active');
            $('.spend__items').find('.box').removeClass('active');
            $('.spend__items').find('.box').eq(index).addClass('active');
        }
    })
}

function initializeTrustSlider() {
    $('.trust__items').slick({
        infinite: true,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<button class="trust__arrow"><svg><use xlink:href="#arrow-ico"></use></svg></button>',
        nextArrow: '<button class="trust__arrow trust__arrow--next"><svg><use xlink:href="#arrow-ico"></use></svg></button>'
    })
}

function mobilePrevent() {
    if($(window).width() < 993) {
        $('.header__nav-item.has-child').addClass('prevent');
        $('.header__nav-item.has-child.prevent a:first-child').on('click', function (e) {
            e.preventDefault();
            $(this).parents('.has-child').toggleClass('open');
        })
    } else {
        $('.header__nav-item.has-child').removeClass('prevent');
    }

}

var slider_init = false;

$(document).ready(function () {
    initMainSlider();
    openMobmenu();
    openTabs();
    mobFooterTabs();
    filterRange();
    initCardSlider();
    openCardHall();
    resultTabs();
    resultCollpse();
    resultShowMore();
    resultShowHall();
    resultTabSliderReInit();
    openResultFilter();
    // popupMap();
    corporateSliderInit();
    corporateTabs();
    eventSliderInit();
    $('select').styler();
    mobilePrevent();
    boxTabsChanger();

    if ($(window).width() < 521) {
        initializeTrustSlider();
    }

    $(window).resize(function () {
        mobilePrevent();
        if ($(window).width() < 521) {
            if (slider_init === false) {
                slider_init = true;
                console.log('init');
                initializeTrustSlider();
            }
        } else {
            slider_init = false;
            $('.trust__items').slick('unslick');
        }
    });

    $(document).ready(function() {
        $("img.lazy").lazyload({
            effect : "fadeIn"
        });
    });

});